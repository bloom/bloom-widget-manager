/*
 * bloom-widget-manager
 *
 * Copyright (C) 2010, Agorabox.
 *
 * Author: BAUBEAU Sylvain
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gtk/gtk.h>
#include <glib/gi18n.h>
#include <QObject>
#include <algorithm>

#include "bloom-widget-manager.h"
#include "bloom-panel-home-dbus-proxy.h"

#include "../config.h"

#include <locale.h>
#define _(String) gettext (String)

#define DEFAULT_GROUP_NAME "Desktop Entry"
#define WIDTH_ICON_WIDTH 64
#define WIDTH_ICON_HEIGHT 64

using namespace std;

class WidgetEntry : public QWidget
{
public:
    WidgetEntry(const char *filename) :
        vlayout(this),
        icon_label(this)
    {
        GError *error = NULL;
        left_button_down = false;
        grabbed_mouse = false;

        key_file = g_key_file_new();
        g_key_file_load_from_file(key_file, filename, G_KEY_FILE_NONE, &error);
        if (error) {
            qDebug() << (const char*) error->message;
            return;
        }

        name = get_value("Name");
        description = get_value("Description");
        icon_path = get_value("Icon");
        service = get_value("X-Bloom-Service");
        schema = get_value("X-Bloom-Schema");
        hidden = get_value("Hidden") == "true";

        g_key_file_free(key_file);

        if (hidden)
            return;

        vlayout.setSpacing(0);
        vlayout.setMargin(0);
        setFixedSize(DIALOG_BUTTON_SIZE, DIALOG_BUTTON_SIZE);
        setStyleSheet("background-color:white;border-radius:5px;border:1px solid #C6C6C6;padding:0px;margin:0px;");
        setContentsMargins(0, 0, 0, 0);

        QFileInfo icon_file_info(QDir(PKGDATADIR "/"), icon_path);
        QString icon_full_path = icon_file_info.absoluteFilePath();

        GtkIconTheme *icon_theme = gtk_icon_theme_get_default();
        GtkIconInfo *icon_info = gtk_icon_theme_lookup_icon(icon_theme,
                                                            icon_path.toLatin1(),
                                                            64,
                                                            GTK_ICON_LOOKUP_USE_BUILTIN);
        if (icon_info) {
            const gchar *icon_filename = gtk_icon_info_get_filename(icon_info);
            if (icon_filename) {
                icon_full_path = icon_filename;
            }
            gtk_icon_info_free(icon_info);
        }

        QIcon icon(icon_full_path);
        icon_label.setPixmap(icon.pixmap(QSize(WIDTH_ICON_WIDTH, WIDTH_ICON_HEIGHT)));
        icon_label.setAlignment(Qt::AlignCenter);
        icon_label.setStyleSheet("background-color:transparent;border-radius:8px;height:auto;width:auto;border:0px;");
        vlayout.addWidget(&icon_label);

        name_label.setText(name);
        name_label.setWordWrap(true);
        name_label.setAlignment(Qt::AlignCenter);
        name_label.setStyleSheet("background-color:transparent;border-radius:8px;height:auto;width:auto;border:0px;font-size:10px;margin-bottom: 2px");
        name_label.adjustSize();
        name_label.setFixedHeight(name_label.height()); //DIALOG_BUTTON_SIZE * 0.33);
        vlayout.addWidget(&name_label);

        setMouseTracking(true);
        setLayout(&vlayout);
        show();
    }

    ~WidgetEntry() {
    }

    QString get_value(QString key) {
        GError *error = NULL;
        gchar *value = g_key_file_get_locale_string(key_file, g_key_file_get_start_group(key_file),
                                                    key.toStdString().c_str(), NULL, &error);
        if (!value) {
            qDebug() << (const char*) error->message;
            return QString("");
        }
        QString str_val = QString::fromUtf8(value);
        g_free(value);
        return str_val;
    }

    void start_drag() {
        setCursor(QCursor(Qt::SizeAllCursor));
        grabMouse();
        grabbed_mouse = true;
    }

    void end_drag() {
        setCursor(QCursor(Qt::ArrowCursor));
        releaseMouse();
    }

    void leaveEvent(QEvent *event) {
        if (left_button_down) {
            end_drag();
            BloomPanelHomeProxy *proxy = new BloomPanelHomeProxy(QString("org.agorabox.Bloom.Panel.Home"),
                                                                 QString("/Home"), QDBusConnection::sessionBus(), this);
            proxy->drop_new_widget(service, schema);
            ((QWidget*) parent())->close();
        }
    }

    void mouseMoveEvent(QMouseEvent *event) {
        QWidget *parent_widget = (QWidget*) parent();
        QPoint rel_pos = mapTo(parent_widget, event->pos());
        if (event->buttons() & Qt::LeftButton) {
            if (!grabbed_mouse) {
                start_drag();
            }
            else if (rel_pos.x() < 0 || rel_pos.y() < 0 ||
                     rel_pos.x() >= parent_widget->width() || rel_pos.y() >= parent_widget->height()) {
                end_drag();
                ((QWidget*) parent())->hide();
            }
        }
    }

    void mousePressEvent(QMouseEvent *event) {
        left_button_down = event->buttons() & Qt::LeftButton;
    }

    void mouseReleaseEvent(QMouseEvent *event) {
        left_button_down = false;
        grabbed_mouse = false;
        setCursor(QCursor(Qt::ArrowCursor));
        releaseMouse();
    }

    QVBoxLayout vlayout;
    QLabel icon_label;
    QLabel name_label;
    bool left_button_down;
    bool grabbed_mouse;

    GKeyFile *key_file;
    QString name;
    QString service;
    QString description;
    QString icon_path;
    QString schema;
    bool hidden;
};

WidgetChooser::WidgetChooser() :
    QDialog(NULL, Qt::Tool | Qt::WindowStaysOnTopHint),
    grid(this),
    label(QString::fromUtf8(_("Please drag and drop a widget into your workspace")))
{
    setWindowTitle(QString::fromUtf8(_("Widget selector")));
    setStyleSheet("background-color:white;");
    label.setAlignment(Qt::AlignHCenter | Qt::AlignTop);
    label.setWordWrap(true);
    label.setStyleSheet("font-size:1.5em;font-weight:bold;");
    
    previous = new QPushButton(this);
    previous->setText("Previous");
    previous->setFocusPolicy(Qt::NoFocus);
    previous->setStyleSheet("background:url("PKGDATADIR"/prev_page.png) no-repeat #D9D9D9 center left;border:0px;padding:4px;border-radius:5px;font-weight:bold;text-align:right;");
    
    next = new QPushButton(this);
    next->setText("Next");
    next->setFocusPolicy(Qt::NoFocus);
    next->setStyleSheet("background:url("PKGDATADIR"/next_page.png) no-repeat #D9D9D9 center right;border:0px;padding:4px;border-radius:5px;font-weight:bold;text-align:left;");

    info_page = new QLabel(QString::fromUtf8(_("Loading...")), this);
    info_page->setStyleSheet("font-weight:bold;");
    info_page->setAlignment(Qt::AlignCenter | Qt::AlignBottom);

    panel_navigation.addWidget(previous);
    panel_navigation.addWidget(info_page);
    panel_navigation.addWidget(next);

    footer.setLayout(&panel_navigation);

    grid.addWidget(&label, 0, 0, 1, DIALOG_BUTTON_NUMBER_WIDTH);
    grid.addWidget(&footer, DIALOG_BUTTON_NUMBER_HEIGHT + 1, 0, 1, DIALOG_BUTTON_NUMBER_WIDTH);

}

void WidgetChooser::previous_page()
{
    this->current_page--;
    if (this->current_page < 1)
        this->current_page = this->nb_pages;
    this->display_page();
}

void WidgetChooser::next_page()
{
    this->current_page++;
    if (this->current_page > this->nb_pages)
        this->current_page = 1;
    this->display_page();
}

WidgetChooser::~WidgetChooser()
{
    qDebug() << "Dialog destroyed";
    while (!entries.isEmpty()) {
        delete entries.first();
        entries.removeFirst();
    }
}

void WidgetChooser::display_page()
{
    QDir widgets_dir(DATADIR "/bloom/widgets");
    int begin = (current_page-1) * (DIALOG_BUTTON_NUMBER_WIDTH * DIALOG_BUTTON_NUMBER_HEIGHT);

    this->info_page->setText(QString::number(current_page) + "/" + QString::number(nb_pages));

    /* Remove old widgets */
    while (!entries.isEmpty()) {
        delete entries.first();
        entries.removeFirst();
    }

    int widget_size = DIALOG_BUTTON_NUMBER_WIDTH;
    for (int n = 0, i = 0; i < DIALOG_BUTTON_NUMBER_WIDTH * DIALOG_BUTTON_NUMBER_HEIGHT; i++) {
        if (i + begin >= widgets.size()) {
            QWidget *widget = new QWidget();
            widget->setFixedSize(DIALOG_BUTTON_SIZE, DIALOG_BUTTON_SIZE);
            widget->setStyleSheet("background-color:white;border-radius:0px;border:1px solid #FFFFFF;");
            entries.append(widget);
            grid.addWidget(widget, n / widget_size + 1, n % widget_size);
            n++;
        }
        else {
            WidgetEntry *widget = new WidgetEntry(widgets_dir.filePath(widgets[i + begin]).toStdString().c_str());
            if (!widget->hidden) {
                entries.append(widget);
                grid.addWidget(widget, n / widget_size + 1, n % widget_size);
                n++;
            }
            else {
                delete widget;
                continue;
            }
        }
    }
}

void WidgetChooser::fill()
{
    QDir widgets_dir(DATADIR "/bloom/widgets");
    widgets = widgets_dir.entryList(QStringList("*.desktop"));
    
    if (!widgets.size()) {
        qDebug() << "No Bloom widget found...";
        return;
    }

    this->connect(this->previous, SIGNAL(clicked()), this, SLOT(previous_page()));
    this->connect(this->next, SIGNAL(clicked()), this, SLOT(next_page()));

    nb_pages = ceil(widgets.size() / (DIALOG_BUTTON_NUMBER_WIDTH * DIALOG_BUTTON_NUMBER_HEIGHT));
    if (widgets.size() % (DIALOG_BUTTON_NUMBER_WIDTH * DIALOG_BUTTON_NUMBER_HEIGHT))
        nb_pages++;
    current_page = 1;

    display_page();
    
    show();
    raise();
    activateWindow();
}

ManagerButton::ManagerButton(QWidget *parent) :
    QPushButton(parent)
{
    connect(this, SIGNAL(clicked()), this, SLOT(on_new_widget()));

    icon = QIcon(PKGDATADIR "/add_bloom_widget.png");
    pixmap = icon.pixmap(64, 64);
    setMask(pixmap.mask());
    resize(64, 64);
}

void ManagerButton::on_new_widget() {
    WidgetChooser *widget_chooser = new WidgetChooser();
    widget_chooser->fill();
    widget_chooser->exec();
}

BloomWidgetManager::BloomWidgetManager() :
    QX11EmbedWidget()
{
    setWindowFlags(Qt::FramelessWindowHint | Qt::Tool);
}

BloomWidgetManager::~BloomWidgetManager()
{
}


int BloomWidgetManager::create(int container, const QString &gconf)
{
    new_widget_button = new ManagerButton(this);
    new_widget_button->setFlat(true);
    new_widget_button->setAttribute(Qt::WA_TranslucentBackground, true);
    new_widget_button->setObjectName("add_bloom_widget_button");
    new_widget_button->setStyleSheet("QPushButton#add_bloom_widget_button {"
                                     " background-image:url(" PKGDATADIR "/add_bloom_widget.png);"
                                     " border: none;"
                                     "}");
    setMask(new_widget_button->pixmap.mask());
    embedInto(container);
    new_widget_button->show();
    show();
    return 0;
}

