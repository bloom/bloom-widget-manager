#include "../config.h"

#include <locale.h>
#include <glib/gi18n.h>

#include "bloom-widget-manager.h"
#include "bloom-widget-dbus-adaptor.h"
#include "bloom-widget-qt.h"

#define _(String) gettext (String)

#define BLOOM_WIDGET_MANAGER_SERVICE "org.agorabox.Bloom.Widget.Manager"

int main(int argc, char **argv) {
    QString object_path;
    QString service_name;
    QDBusConnection connection = QDBusConnection::sessionBus();

    bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR);
    bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
    textdomain(GETTEXT_PACKAGE);

    QApplication app(argc, argv);
    if (argc == 2 && strcmp(argv[1], "-s") == 0) {
        BloomWidgetManager standAlone;
        standAlone.create(0, 0);
        return app.exec();
    }

    BloomWidgetManager *widget = new BloomWidgetManager();
    new WidgetAdaptor(widget);

    QString service = get_free_service_name(BLOOM_WIDGET_MANAGER_SERVICE);
    connection.registerService(service);
    connection.registerObject("/Widget", widget);

    return app.exec();
}

