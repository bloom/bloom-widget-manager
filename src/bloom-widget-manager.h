#ifndef BLOOM_WIDGET_MANAGER_H_
#define BLOOM_WIDGET_MANAGER_H_

#include <QtCore>
#include <QtGui>
#include <QUrl>
#include <QtDBus/QDBusObjectPath>
#include <QX11EmbedWidget>

class WidgetChooser;

class ManagerButton : public QPushButton
{
    Q_OBJECT

public:
    ManagerButton(QWidget *parent = NULL);

public Q_SLOTS:
    void on_new_widget();

public:
    QIcon icon;
    QBitmap mask;
    QPixmap pixmap;
    QPushButton button;
};

class BloomWidgetManager : public QX11EmbedWidget
{
    Q_OBJECT

public:
    BloomWidgetManager();
    ~BloomWidgetManager();

public Q_SLOTS:
    int create(int container, const QString &gconf);

protected:
    ManagerButton     *new_widget_button;
};

#define DIALOG_BUTTON_SIZE              100
#define DIALOG_BUTTON_NUMBER_WIDTH      3
#define DIALOG_BUTTON_NUMBER_HEIGHT     4
class WidgetEntry;
class WidgetChooser : public QDialog
{
    Q_OBJECT

public:
    WidgetChooser();
    virtual ~WidgetChooser();
    void display_page();
    void fill();

protected:
    /* Containers */
    QList<QWidget *> entries;
    QStringList         widgets;
    /* Page management */
    int                 current_page;
    int                 nb_pages;
    /* Main layout */
    QGridLayout         grid;
    /* Title */
    QLabel              label;
    /* Footer */
    QHBoxLayout         panel_navigation;
    QWidget             footer;
    QPushButton         *previous;
    QPushButton         *next;
    QLabel              *info_page;
public Q_SLOTS:
    void                previous_page();
    void                next_page();
    
};

#endif /* BLOOM_WIDGET_MANAGER_H_ */
